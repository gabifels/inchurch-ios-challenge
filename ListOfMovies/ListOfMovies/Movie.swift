//
//  Movie.swift
//  ListOfMovies
//
//  Created by Gabi Rocha on 04/04/20.
//  Copyright © 2020 GabiRocha. All rights reserved.
//

import Foundation

struct ResultAPI: Codable {
    let results: [Movie]?
    let total_results: Int?
    let page: Int?
    let total_pages: Int?
    
}

struct Movie: Codable {
    let id: Int?
    let poster_path: String?
    let overview: String?
    let release_date: String?
    let genre_ids: [Int]?
    let title: String?
    let vote_average: Double?
    
    func getURLImage(path: String) -> URL {
        let url = URL(string: "https://image.tmdb.org/t/p/w500" + path)!
        return url
    }
}
    
struct Genre: Codable {
    var genres: [GenreData]?
}

struct GenreData: Codable {
    var id: Int?
    var name: String?
}




