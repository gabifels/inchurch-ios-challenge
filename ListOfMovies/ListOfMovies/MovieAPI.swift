//
//  MovieAPI.swift
//  ListOfMovies
//
//  Created by Gabi Rocha on 05/04/20.
//  Copyright © 2020 GabiRocha. All rights reserved.
//

import Foundation
import Alamofire

class MovieAPI {
    
    static private let privateKey = "d142f34099649e3fe4e49f390e305671"
    static private let basePath = "https://api.themoviedb.org/3/movie/popular?"
    static private let genrePath = "https://api.themoviedb.org/3/genre/movie/list?"
    static private let language = "en-US"

    
    
    class func getPopularMovies(page: Int, onComplete: @escaping (ResultAPI?) -> Void) {
        let params = ["api_key": privateKey, "page": page] as [String: Any]
        AF.request(basePath, method: .get, parameters: params).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            do {
                let movieData = try JSONDecoder().decode(ResultAPI.self, from: data)
                onComplete(movieData)
                return
            } catch {
                    onComplete(nil)
                    print(error.localizedDescription)
                }
        }  
    }
    
    
    class func getGenre(genresMovieDatail: [Int], onComplete: @escaping ([String]?) -> Void) {

        let params = ["api_key": privateKey, "language": language] as [String: Any]
        AF.request(genrePath, method: .get, parameters: params).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            do {
                var genresNames: [String] = []
                let genresInfo = try JSONDecoder().decode(Genre.self, from: data)
                let genresData = genresInfo.genres!

                for itemArrayMovieDatail in genresMovieDatail {
                    for item in genresData {
                        if itemArrayMovieDatail == item.id {
                            genresNames.append(item.name!)
                        }
                    }
                }
                onComplete(genresNames)
                return
            } catch {
                    onComplete(nil)
                    print(error.localizedDescription)
                }
        }
    }

/*
    static private let movieDatailPath = "https://api.themoviedb.org/3/movie/{movie_id}?"
     
    class func getMovieDetail(id: Int, onComplete: @escaping (Movie?) -> Void) {
        let params = ["api_key": privateKey, "language": language] as [String: Any]
        let url = movieDatailPath + "\(id)?"
        AF.request(url, method: .get, parameters: params).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            do {
                let movieDetailData = try JSONDecoder().decode(Movie.self, from: data)
                onComplete(movieDetailData)
                return
            } catch {
                    onComplete(nil)
                    print(error.localizedDescription)
                }
        }
    }
 */
}


