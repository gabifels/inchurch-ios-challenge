//
//  MoviesTableViewController.swift
//  ListOfMovies
//
//  Created by Gabi Rocha on 04/04/20.
//  Copyright © 2020 GabiRocha. All rights reserved.
//

import UIKit

class MoviesTableViewController: UITableViewController {

    var movies: [Movie] = []
    var loadingMovie = false
    var currentPage = 1
    var total = 0
    var pageTotal = 0

    
    func loadMovie() {
        loadingMovie = true
        MovieAPI.getPopularMovies(page: currentPage) { (movieData) in
            if let movieData = movieData {
                self.movies = movieData.results!
                self.total = movieData.total_results!
                self.pageTotal = movieData.total_pages!
                DispatchQueue.main.async {
                    self.loadingMovie = false
                    self.tableView.reloadData()

                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMovie()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count * self.currentPage
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
        let movie = movies[indexPath.row]
        cell.prepareCell(with: movie)
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == movies.count && !loadingMovie && self.currentPage != self.pageTotal {
            self.currentPage += 1
            self.loadMovie()
        }
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let movieDetailVC = segue.destination as! MovieDetailViewController
        movieDetailVC.movie = movies[tableView.indexPathForSelectedRow!.row]
        
    }
    
    
    
    
}
