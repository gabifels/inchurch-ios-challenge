//
//  MovieDetailViewController.swift
//  ListOfMovies
//
//  Created by Gabi Rocha on 04/04/20.
//  Copyright © 2020 GabiRocha. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbOverview: UILabel!
    @IBOutlet weak var lbGenre: UILabel!
    @IBOutlet weak var btfavorite: UIButton!
    @IBOutlet weak var ivImage: UIImageView!
    
    var movie: Movie!
    var genre = [GenreData]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMovie(with: movie)
}

    
    func loadMovie(with someMovie: Movie) {
        
        if let title = movie.title {
            self.lbTitle.text = title
        } else { self.lbTitle.text = " --- " }

        if let overview = movie.overview {
            self.lbOverview.text = overview
        } else { self.lbOverview.text = " --- " }
        
        if let genre = movie.genre_ids {
            MovieAPI.getGenre(genresMovieDatail: genre) { (genresNames) in
                if let genresNames = genresNames {
                    let geners: String = genresNames.joined(separator: ", ")
                    self.lbGenre.text = geners
                }
            }
        } else { lbGenre.text = " --- " }
        
        
        if let path = movie.poster_path {
            let url = movie.getURLImage(path: path)
            self.ivImage.kf.indicatorType = .activity
            self.ivImage.kf.setImage(with: url)
        } else {
            self.ivImage.image = UIImage(named:"error.png")
        }
    }
}
