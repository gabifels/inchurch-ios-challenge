//
//  MovieTableViewCell.swift
//  ListOfMovies
//
//  Created by Gabi Rocha on 04/04/20.
//  Copyright © 2020 GabiRocha. All rights reserved.
//

import UIKit
import Kingfisher

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbRelease: UILabel!
    @IBOutlet weak var lbVote: UILabel!
    @IBOutlet weak var ivImage: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func prepareCell(with movie: Movie){
    
        if let title = movie.title {
            self.lbTitle.text = title
        } else { lbTitle.text = " - " }
       
        if let realese = movie.release_date  {
            self.lbRelease.text = "Estreia: " + realese
        } else { lbRelease.text = " - " }
       
        if let vote = movie.vote_average {
            self.lbVote.text = "Nota: \(vote)"
        } else { lbVote.text = " - " }
        
        if let path = movie.poster_path {
            let url = movie.getURLImage(path: path)
            self.ivImage.kf.indicatorType = .activity
            self.ivImage.kf.setImage(with: url)
        } else {
            self.ivImage.image = UIImage(named:"error.png")
        }
        
    }
}
